#!/bin/bash
#
# This file is supposed to be executed inside a Vagrant box,
# when you want to pull the database from Vagrant, and see that
# in your working directory.
#

# exit in case of any error
set -e

FILE="$1"
if [ -z "$FILE" ]; then
	echo "Usage:"
	echo "  $0 file.sql"
	exit 1
fi

# exporting schema and one-row per data
sudo mysqldump --extended-insert=FALSE ldto > "$FILE"

# stripping e-mail addresses
sed "s/'[a-z\.\-]*@[a-z\.\-]*'/NULL/g" -i "$FILE"

# stripping passwords (now are SHA1 salted)
sed "s/'[a-f0-9]\{40\}'/NULL/g" -i "$FILE"
